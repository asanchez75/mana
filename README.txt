MIGRATE COMMANDS

drush migrate-upgrade --configure-only
drush cex --destination=/tmp/export
cp /tmp/export/migrate_plus.migration.* /tmp/export/migrate_plus.migration_group.migrate_*.yml mana/config/install/
drush cim --partial --source=/Library/WebServer/Documents/test8/modules/custom/mana/config/install/

FOR DEBUGGING

a) Snippets

$node = \Drupal\node\Entity\Node::load(1);
dpm($node);
$fc = \Drupal\field_collection\Entity\FieldCollectionItem::load(1);
dpm($fc);
$node = \Drupal\node\Entity\Node::load(1);
$fc = \Drupal\field_collection\Entity\FieldCollectionItem::create(['field_name' => 'field_personal_information']);
$fc->field_first_name->setValue('Rosa');
$fc->field_last_name->setValue('Tenorio');
$fc->field_address->setValue('Calle Jordan');
$fc->setHostEntity($node);
$fc->save();

b) For displaying $query inside migrate source class

print strtr((string) $query, $query->arguments());

REFERENCE LINKS

https://www.drupal.org/node/2683787
https://www.drupal.org/docs/8/api/migrate-api/migrate-process-plugins/process-plugin-iterator
https://www.drupal.org/docs/8/api/migrate-api/migrate-process/process-plugin-extract
https://www.drupal.org/node/2757989
https://www.drupal.org/docs/8/api/migrate-api/migrate-process-plugins/process-plugin-migration_lookup-formerly-migration

c) To check error migration messages

drush mmsg upgrade_d7_file

FIELD COLLECTION

https://bitbucket.org/asanchez75/field_collection/commits/branch/mana
